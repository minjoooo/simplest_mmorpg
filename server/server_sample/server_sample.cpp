#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <string>
#include <map>
#include <set>
#include <queue>
#include <thread>
#include <mutex>
#include <chrono>
#include <codecvt>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

using namespace std;
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "lua53.lib")

#include "aStar.h"
#include "DataBase.h"
#include "protocol.h"

#define MAX_BUFFER        1024
constexpr auto VIEW_RANGE = 9;
constexpr auto RUN_RANGE = 5;	// 0~
constexpr auto MOVE_RANGE = 5;

enum EVENT_TYPE { EV_RECV, EV_SEND, EV_MOVE, EV_MOVE_RANGE, EV_PLAYER_MOVE_NOTIFY, EV_MOVE_TARGET,
	EV_ATTACK, EV_NPC_ATTACK, EV_HEAL, EV_REBIRTH, EV_CAN_MOVE, EV_CAN_ATTACK };

enum MSG_TYPE { MSG_KILL, MSG_ATTACK, MSG_HIT, MSG_DIE };

struct OVER_EX {
	WSAOVERLAPPED	over;
	WSABUF			wsabuf[1];
	char			net_buf[MAX_BUFFER];
	EVENT_TYPE		evnet_type;
};

struct SOCKETINFO
{
	OVER_EX			recv_over;
	SOCKET			socket;
	int				id;
	int				hp;
	int				level;
	int				exp;

	bool			is_alive;
	bool			is_move;
	bool			is_attack;
	bool			is_active;

	int				monster_type;
	int				count;
	int				target;

	short			sx, sy;	//startx starty
	short			x, y;
	std::wstring	name;

	set <int>		near_id;
	mutex			near_lock;

	lua_State* L;
	mutex vm_lock;
};

struct EVENT {
	int obj_id;
	chrono::high_resolution_clock::time_point wakeup_time;
	int event_type;
	int target_obj;

	constexpr bool operator <( const EVENT& lh )const
	{
		return wakeup_time > lh.wakeup_time;
	}
};

priority_queue<EVENT> timer_queue;
mutex timer_lock;

DataBase* database;

map <int, SOCKETINFO*> clients;
HANDLE	g_iocp;

int new_user_id { 0 };

Map World;

std::wstring stringToWstring( const std::string& t_str )
{
	typedef codecvt_utf8<wchar_t> convert_type;
	wstring_convert<convert_type, wchar_t> converter;

	return converter.from_bytes( t_str );
}

std::string WstringTostring( const std::wstring& t_str )
{
	using convert_typeX = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes( t_str );
}

void error_display( const char* msg, int err_no )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		( LPTSTR )&lpMsgBuf, 0, NULL );
	cout << msg;
	wcout << L"에러 " << lpMsgBuf << endl;
	while ( true );
	LocalFree( lpMsgBuf );
}

void add_timer( EVENT& ev )
{
	timer_lock.lock();
	timer_queue.push( ev );
	timer_lock.unlock();
}

bool is_Lava( int x, int y )
{
	return ( World( x, y ) == 9 );
}

bool is_NPC( int id )
{
	return id >= NPC_ID_START;
}

bool is_Scorpion( int id )
{
	return clients[id]->monster_type == MON_SCORPION;
}

bool is_Move( int id )
{
	return clients[id]->is_move;
}

bool is_Active( int npc_id )
{
	return clients[npc_id]->is_active;
}

bool is_Alive( int npc_id )
{
	return clients[npc_id]->is_alive;
}

bool is_attack_range( int a, int b )
{
	return ( abs( clients[a]->x - clients[b]->x ) + abs( clients[a]->y - clients[b]->y ) <= 1 );
}

bool is_range_move( int id, int x, int y )
{
	return ( abs( clients[id]->x - x ) + abs( clients[id]->y - y ) <= MOVE_RANGE );
}

bool is_near( int a, int b )
{
	return ( abs( clients[a]->x - clients[b]->x ) < VIEW_RANGE && abs( clients[a]->y - clients[b]->y ) < VIEW_RANGE );
}
bool is_near_NPC( int a, int b )
{
	if ( VIEW_RANGE + VIEW_RANGE < abs( clients[a]->x - clients[b]->x ) ) return false;
	if ( VIEW_RANGE + VIEW_RANGE < abs( clients[a]->y - clients[b]->y ) ) return false;
	return true;
}

bool is_npc_in_near( int b )
{
	for ( auto& obj : clients )
	{
		if ( true == is_near( b, obj.second->id ) )
			return true;
	}
	return false;
}

void add_player_heal( int id )
{
	if ( false == is_NPC( id ) && false == is_Active( id ) )
	{
		clients[id]->is_active = true;
		EVENT ev { id,chrono::high_resolution_clock::now() + 5s, EV_HEAL, 0 };
		add_timer( ev );
	}
}

void add_move_range( int id )
{
	if ( true == is_NPC( id ) && true == is_Scorpion( id ) && false == is_Move( id ) )
	{
		clients[id]->is_move = true;
		EVENT ev { id,chrono::high_resolution_clock::now() + 1s, EV_MOVE_RANGE, 0 };
		add_timer( ev );
	}
}

void add_npc_random_move( int target_id, int npc_id )
{
	if ( true == is_NPC( npc_id ) && false == is_Active( npc_id ) )
	{
		clients[npc_id]->is_active = true;
		clients[npc_id]->target = target_id;
		EVENT ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_MOVE, 0 };
		add_timer( ev );
	}
}

void add_npc_target_move( int target_id, int npc_id )
{
	if ( true == is_NPC( npc_id ) && false == is_Active( npc_id ) )
	{
		clients[npc_id]->is_active = true;
		clients[npc_id]->target = target_id;
		EVENT ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_MOVE_TARGET, 0 };
		add_timer( ev );
	}
}

void add_npc_attack_player( int target_id, int npc_id )
{
	if ( true == is_NPC( npc_id ) && false == is_Active( npc_id ) )
	{
		clients[npc_id]->is_active = true;
		clients[npc_id]->target = target_id;
		EVENT ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_NPC_ATTACK, 0 };
		add_timer( ev );
	}
}

int get_power_by_level( int id )
{
	return clients[id]->level * 2;
}

int get_exp_by_level( int id )	//npc id
{
	return pow( 2, clients[id]->level ) * 5;
}

void send_packet( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	int packet_size = packet[0];

	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0x00, sizeof( OVER_EX ) );
	send_over->evnet_type = EV_SEND;
	memcpy( send_over->net_buf, packet, packet_size );
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;

	int ret = WSASend( clients[id]->socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0 );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			error_display( "WSARecv Error :", err_no );
	}
}

void send_login_ok_packet( int id )
{
	sc_packet_login_ok packet;
	packet.id = id;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_OK;
	packet.x = clients[id]->x;
	packet.y = clients[id]->y;
	packet.hp = clients[id]->hp;
	packet.level = clients[id]->level;
	packet.exp = clients[id]->exp;
	send_packet( id, &packet );
}

void send_login_deny_packet( int id )
{
	sc_packet_login_fail packet;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_FAIL;
	send_packet( id, &packet );
}

void send_put_player_packet( int client, int new_id )
{
	sc_packet_put_object packet;
	packet.id = new_id;
	packet.size = sizeof( packet );
	packet.type = SC_PUT_OBJECT;
	packet.x = clients[new_id]->x;
	packet.y = clients[new_id]->y;
	packet.obj_type = clients[new_id]->monster_type;
	send_packet( client, &packet );

	if ( client == new_id ) return;
	lock_guard <mutex> lg { clients[client]->near_lock };
	clients[client]->near_id.insert( new_id );
}

void send_pos_packet( int client, int mover )
{
	sc_packet_pos packet;
	packet.id = mover;
	packet.size = sizeof( packet );
	packet.type = SC_POS;
	packet.x = clients[mover]->x;
	packet.y = clients[mover]->y;

	clients[client]->near_lock.lock();
	if ( 0 != clients[client]->near_id.count( mover ) ) {
		clients[client]->near_lock.unlock();
		send_packet( client, &packet );
	}
	else {
		clients[client]->near_lock.unlock();
		send_put_player_packet( client, mover );
	}
}

void send_remove_player_packet( int client, int leaver )
{
	sc_packet_remove_object packet;
	packet.id = leaver;
	packet.size = sizeof( packet );
	packet.type = SC_REMOVE_OBJECT;
	send_packet( client, &packet );

	lock_guard <mutex> lg { clients[client]->near_lock };
	clients[client]->near_id.erase( leaver );
}

void send_chat_packet( int client, int chatter, char  mess[] )
{
	sc_packet_chat packet;
	packet.id = chatter;
	packet.size = sizeof( packet );
	packet.type = SC_CHAT;
	strcpy_s( packet.chat, mess );
	send_packet( client, &packet );
}

void send_global_chat_packet( int client, char  mess[] )
{
	sc_packet_global_chat packet;
	packet.size = sizeof( packet );
	packet.type = SC_GLOBAL_CHAT;
	strcpy_s( packet.chat, mess );
	send_packet( client, &packet );
}

void send_set_player_state_packet( int id, char state_type, int num )
{
	sc_packet_set_player_state packet;
	packet.size = sizeof( packet );
	packet.type = SC_PLAYER_STATE_CHAGE;
	packet.state_type = state_type;
	packet.num = num;
	send_packet( id, &packet );
}

void do_teleport( int id, short x, short y )
{
	clients[id]->near_lock.lock();
	auto old_vl = clients[id]->near_id;
	clients[id]->near_lock.unlock();

	clients[id]->x = x;
	clients[id]->y = y;

	set <int> new_vl;
	for ( auto& cl : clients ) {
		int other = cl.second->id;
		if ( id == other )
			continue;
		if ( true == is_near_NPC( id, other ) )
		{
			add_move_range( other );
			if ( true == is_near( id, other ) )
			{
				new_vl.insert( other );
			}
		}
	}


	send_pos_packet( id, id );
	for ( auto cl : old_vl ) {
		if ( 0 != new_vl.count( cl ) ) {
			if ( false == is_NPC( cl ) )
				send_pos_packet( cl, id );
		}
		else if ( id != cl )
		{
			send_remove_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_remove_player_packet( cl, id );
		}
	}
	for ( auto cl : new_vl ) {
		if ( 0 == old_vl.count( cl ) && true == is_Alive( cl ) ) ///
		{
			send_put_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_put_player_packet( cl, id );
		}
	}

}

void player_heal( int id )
{
	clients[id]->is_active = false;
	bool full = false;
	//10% heal
	clients[id]->hp += clients[id]->level;
	if ( clients[id]->hp >= clients[id]->level * 10 )
	{
		clients[id]->hp = clients[id]->level * 10;
		full = true;
	}

	//send state
	send_set_player_state_packet( id, STATE_HP, clients[id]->hp );

	//send msg
	string msg {};
	msg += "!HEAL!";
	char mess[50];
	strcpy_s( mess, msg.c_str() );
	send_global_chat_packet( id, mess );

	//더 힐해야하면 event 추가
	if ( false == full )
	{
		add_player_heal( id );
	}
}

void player_level_up( int id )
{
	string msg {};
	msg += "!LEVEL UP!";
	char mess[50];
	strcpy_s( mess, msg.c_str() );
	send_global_chat_packet( id, mess );

	//exp0으로
	clients[id]->exp = 0;
	//level+=1
	clients[id]->level += 1;
	//DB에 쓴다
	database->SetPos( clients[id]->name, clients[id]->x, clients[id]->y, clients[id]->level, clients[id]->exp );

	// hp 풀피로
	clients[id]->hp = clients[id]->level * 10;

	send_set_player_state_packet( id, STATE_LEVEL, clients[id]->level );
}

void earn_exp( int id, int exp, bool double_exp )
{
	if ( double_exp )
		clients[id]->exp += exp * 2;
	else
		clients[id]->exp += exp;


	if ( clients[id]->exp >= ( pow( 2, clients[id]->level ) * 100 ) )
	{
		player_level_up( id );
	}

	send_set_player_state_packet( id, STATE_EXP, clients[id]->exp );
}


void makeMessageandSend( int to, int from, MSG_TYPE type )
{
	// 플레이어가 npc공격
	std::string msg {};

	msg += WstringTostring( clients[to]->name );

	switch ( type )
	{
	case MSG_ATTACK:
		msg += " attack ";
		break;
	case MSG_HIT:
		msg += " hit by ";
		break;
	case MSG_KILL:
		msg += " kill ";
		break;
	case MSG_DIE:
		msg += "die!";
		break;
	}
	if ( type != MSG_DIE )
	{
		bool double_exp = false;
		switch ( clients[from]->monster_type )
		{
		case MON_BOSS:
			msg += "BOSS ";
			double_exp = true;
			break;
		case MON_RABBIT:
			msg += "rabbit ";
			break;
		case MON_ZOMBIE:
			msg += "zombie ";
			double_exp = true;
			break;
		case MON_SCORPION:
			msg += "scorpion ";
			double_exp = true;
			break;
		}
		msg += ": ";
		switch ( type )
		{
		case MSG_ATTACK:
			msg += std::to_string( get_power_by_level( to ) );
			msg += " damage!";
			break;
		case MSG_HIT:
			msg += std::to_string( get_power_by_level( from ) );
			msg += " damage!";
			break;
		case MSG_KILL:
		{
			int exp = get_exp_by_level( from );
			earn_exp( to, exp, double_exp );

			if ( double_exp )
				msg += std::to_string( exp * 2 );
			else
				msg += std::to_string( exp );
			msg += " EXP!";
			break;
		}
		}
	}

	char mess[50];
	strcpy_s( mess, msg.c_str() );
	send_global_chat_packet( to, mess );
}
void makeMessageandSend( int to, int from, MSG_TYPE type, bool npc_attack )
{
	//npc가 플레이어 공격
	//to : 공격당한 플레이어
	//from : 공격한 npc
	std::string msg {};

	msg += WstringTostring( clients[to]->name );

	switch ( type )
	{
	case MSG_ATTACK:
		msg += " attack ";
		break;
	case MSG_HIT:
		msg += " hit by ";
		break;
	case MSG_KILL:
		msg += " kill ";
		break;
	case MSG_DIE:
		msg += "die!";
		break;
	}
	if ( type != MSG_DIE )
	{
		switch ( clients[from]->monster_type )
		{
		case MON_BOSS:
			msg += "BOSS ";
			break;
		case MON_RABBIT:
			msg += "rabbit ";
			break;
		case MON_ZOMBIE:
			msg += "zombie ";
			break;
		case MON_SCORPION:
			msg += "scorpion ";
			break;
		}
		msg += ": ";
		switch ( type )
		{
		case MSG_ATTACK:
			msg += std::to_string( get_power_by_level( to ) );
			msg += " damage!";
			break;
		case MSG_HIT:
			msg += std::to_string( get_power_by_level( from ) );
			msg += " damage!";
			break;
		case MSG_KILL:
			msg += std::to_string( get_power_by_level( to ) );
			msg += " EXP!";
			break;
		}
	}

	char mess[50];
	strcpy_s( mess, msg.c_str() );
	send_global_chat_packet( to, mess );
}

bool is_near_id( int player, int other )
{
	lock_guard <mutex> gl { clients[player]->near_lock };
	return ( 0 != clients[player]->near_id.count( other ) );
}

void character_die( int id )
{
	if ( true == is_NPC( id ) )///npc
	{
		clients[id]->is_alive = false;
		// removepla 보내기
		for ( auto& pc : clients )
		{
			if ( true == is_NPC( pc.second->id ) ) continue;
			if ( false == is_near( pc.second->id, id ) ) continue;
			send_remove_player_packet( pc.second->id, id );
		}

		// 30초 후 살아나는 이벤트 걸기
		clients[id]->is_active = true;
		clients[id]->target = NULL;
		EVENT ev { id,chrono::high_resolution_clock::now() + 30s, EV_REBIRTH, 0 };
		add_timer( ev );
	}
	else	//플레이어가 죽었따
	{
		// 포지션 디비 저장된걸로
		do_teleport( id, clients[id]->sx, clients[id]->sy );

		//exp 반 깎아
		clients[id]->exp /= 2;

		//hp 풀로 해
		clients[id]->hp = clients[id]->level * 10;

		//exp 바뀐거 클라한테 보내줘
		send_set_player_state_packet( id, STATE_HP, clients[id]->hp );
		send_set_player_state_packet( id, STATE_EXP, clients[id]->exp );

		//db에 셋해줘
		database->SetPos( clients[id]->name, clients[id]->x, clients[id]->y, clients[id]->level, clients[id]->exp );
	}
}

void damage( int id, int damage )
{
	int cur_hp = clients[id]->hp;
	cur_hp -= damage;

	//npc가 공격 당함
	if ( cur_hp <= 0 )
	{
		cur_hp = 0;
		character_die( id );
		makeMessageandSend( clients[id]->target, id, MSG_KILL );
	}
	else
	{
		makeMessageandSend( clients[id]->target, id, MSG_ATTACK );
	}

	clients[id]->hp = cur_hp;
}

bool damage( int id, int from, int damage )
{
	//id : 공격당한 플레이어
	//from : 공격한 npc
	int cur_hp = clients[id]->hp;
	cur_hp -= damage;

	// 플레이어가 공격당함
	if ( cur_hp <= 0 )
	{
		cur_hp = 0;
		character_die( id );
		makeMessageandSend( id, from, MSG_DIE, true );

		return true;//으앙 쥬금
	}
	else
	{
		makeMessageandSend( id, from, MSG_HIT, true );
		clients[id]->hp = cur_hp;
		send_set_player_state_packet( id, STATE_HP, cur_hp );

		add_player_heal( id );

		return false;
	}
}

void ProcessIDPacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );

	std::string stringID {};
	for ( int i = 0; i < packet[0] - 2; ++i )
	{
		stringID += packet[2 + i];
	}
	if ( stringID == "_dummy" )
	{
	dummyGT:
		int x = rand() % WORLD_WIDTH;
		int y = rand() % WORLD_HEIGHT;

		if ( true == is_Lava( x, y ) ) goto dummyGT;

		clients[id]->x = x;
		clients[id]->y = y;

		clients[id]->sx = x;
		clients[id]->sy = y;
	}
	else
	{
		std::wstring wstringID = stringToWstring( stringID );

		int posX, posY, level, exp;
		database->GetPos( wstringID, &posX, &posY, &level, &exp );

		if ( posX == -1 && posY == -1 )
		{
			std::cout << "deny!" << std::endl;
			send_login_deny_packet( id );
			return;
		}
		else
		{
			clients[id]->x = posX;
			clients[id]->y = posY;
			clients[id]->sx = posX;
			clients[id]->sy = posY;
			clients[id]->is_active = false;
			clients[id]->is_alive = true;
			clients[id]->is_move = false;
			clients[id]->id = id;
			clients[id]->name = wstringID;

			clients[id]->level = level;
			clients[id]->exp = exp;
			clients[id]->hp = level * 10;
		}
	}

	send_login_ok_packet( id );

	for ( auto& cl : clients )
	{
		int other_player = cl.first;

		if ( true == is_near_NPC( other_player, id ) )
		{
			add_move_range( other_player );
		}
		if ( true == is_near( other_player, id ) ) {
			if ( false == is_NPC( other_player ) )
				send_put_player_packet( other_player, id );
			if ( other_player != id ) {
				send_put_player_packet( id, other_player );
			}
		}
	}
}

void ProcessTeleportPacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );

	clients[id]->near_lock.lock();
	auto old_vl = clients[id]->near_id;
	clients[id]->near_lock.unlock();

PTPRM:
	short x = rand() % 800;
	short y = rand() % 800;

	if ( true == is_Lava( x, y ) )goto PTPRM;

	clients[id]->x = x;
	clients[id]->y = y;

	set <int> new_vl;
	for ( auto& cl : clients ) {
		int other = cl.second->id;
		if ( id == other )
			continue;
		if ( true == is_near_NPC( id, other ) )
		{
			add_move_range( other );
			if ( true == is_near( id, other ) )
			{
				new_vl.insert( other );
			}
		}
	}

	send_pos_packet( id, id );
	for ( auto cl : old_vl ) {
		if ( 0 != new_vl.count( cl ) ) {
			if ( false == is_NPC( cl ) )
				send_pos_packet( cl, id );
		}
		else if ( id != cl )
		{
			send_remove_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_remove_player_packet( cl, id );
		}
	}
	for ( auto cl : new_vl ) {
		if ( 0 == old_vl.count( cl ) && true == is_Alive( cl ) ) ///
		{
			send_put_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_put_player_packet( cl, id );
		}
	}

}

void ProcessMovePacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	short x = clients[id]->x;
	short y = clients[id]->y;

	if ( x < 0 || y < 0 )return;

	clients[id]->near_lock.lock();
	auto old_vl = clients[id]->near_id;
	clients[id]->near_lock.unlock();

	switch ( packet[2] )
	{
	case DIR_UP: if ( y > 0 ) y--;
		break;
	case DIR_DOWN: if ( y < WORLD_HEIGHT - 1 ) y++;
		break;
	case DIR_LEFT: if ( x > 0 ) x--;
		break;
	case DIR_RIGHT: if ( x < WORLD_WIDTH - 1 ) x++;
		break;
	default: cout << "Invalid Packet Type Error\n";
		while ( true );
	}

	if ( true == is_Lava( x, y ) )return;

	clients[id]->x = x;
	clients[id]->y = y;

	set <int> new_vl;
	for ( auto& cl : clients ) {
		int other = cl.second->id;
		if ( id == other )
			continue;
		if ( true == is_near_NPC( id, other ) )
		{
			add_move_range( other );
			if ( true == is_near( id, other ) )
			{
				new_vl.insert( other );
				if ( true == is_NPC( other ) && false == is_Active( other ) && true == is_Alive( other ) )
				{
					OVER_EX* over_ex = new OVER_EX;
					over_ex->evnet_type = EV_PLAYER_MOVE_NOTIFY;
					*( int* )( over_ex->net_buf ) = id;	//전해줄데가 없어서 여기에 넣어서 전한다
					PostQueuedCompletionStatus( g_iocp, 1, other, &over_ex->over );
				}
			}
		}
	}


	send_pos_packet( id, id );
	for ( auto cl : old_vl ) {
		if ( 0 != new_vl.count( cl ) ) {
			if ( false == is_NPC( cl ) )
				send_pos_packet( cl, id );
		}
		else if ( id != cl )
		{
			send_remove_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_remove_player_packet( cl, id );
		}
	}
	for ( auto cl : new_vl ) {
		if ( 0 == old_vl.count( cl ) && true == is_Alive( cl ) ) ///
		{
			send_put_player_packet( id, cl );
			if ( false == is_NPC( cl ) )
				send_put_player_packet( cl, id );
		}
	}
}

void ProcessBattlePacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );

	switch ( packet[1] )	//지금은 필요없는 case문 나중을 위해서!
	{
	case CS_ATTACK:
	{
		clients[id]->near_lock.lock();
		auto vl = clients[id]->near_id;
		clients[id]->near_lock.unlock();

		for ( auto npc_id : vl )
		{
			if ( true == is_attack_range( npc_id, id ) )
			{
				OVER_EX* over_ex = new OVER_EX;
				over_ex->evnet_type = EV_ATTACK;
				*( int* )( over_ex->net_buf ) = npc_id;	//전해줄데가 없어서 여기에 넣어서 전한다
				PostQueuedCompletionStatus( g_iocp, 1, id, &over_ex->over );
			}
		}
		break;
	}
	default: cout << "Invalid Packet Type Error\n";
		while ( true );
	}
}

void do_target_move( int npc_id )
{
	if ( 0 == clients.count( npc_id ) )
	{
		std::cout << "npc : " << npc_id << "dose not exsist" << std::endl;
		while ( true );
	}

	if ( false == is_NPC( npc_id ) )
	{
		std::cout << "id : " << npc_id << "is not npc" << std::endl;
		while ( true );
	}

	bool player_exists = false;
	for ( int i = 0; i < NPC_ID_START; ++i )
	{
		if ( 0 == clients.count( i ) ) continue;
		if ( true == is_near_NPC( i, npc_id ) )
		{
			player_exists = true;
			break;
		}
	}
	if ( false == player_exists )
	{
		clients[npc_id]->is_active = false;
		return;
	}

	SOCKETINFO* npc = clients[npc_id];
	int x = npc->x;
	int y = npc->y;
	set <int> old_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			old_view_list.insert( obj.second->id );
	}

	point s( x, y ), e( clients[npc->target]->x, clients[npc->target]->y );
	aStar as;

	if ( as.search( s, e, World ) ) {
		std::list<point> path;
		as.path( path );

		std::list<point>::iterator i = path.begin();
		++i;
		x = ( *i ).x;
		y = ( *i ).y;
	}

	if ( npc->x == x && npc->y == y )// 플레이어 옆으로 이동 다 했으면!
	{
		//if ( clients[npc_id]->count > 0 )
		//{
		//	--clients[npc_id]->count;
		EVENT new_ev { npc_id,chrono::high_resolution_clock::now() + 50ms, EV_NPC_ATTACK, 0 };
		add_timer( new_ev );

		clients[npc_id]->is_active = false;
		clients[npc_id]->count = RUN_RANGE;
		//	clients[npc_id]->target = NULL;
		//}
		return;
	}

	npc->x = x;
	npc->y = y;
	set <int> new_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			new_view_list.insert( obj.second->id );
	}
	for ( auto& pc : clients ) {
		if ( true == is_NPC( pc.second->id ) ) continue;
		if ( false == is_near( pc.second->id, npc_id ) ) continue;
		if ( false == is_Alive( npc_id ) ) continue;///
		send_pos_packet( pc.second->id, npc_id );
	}

	if ( clients[npc_id]->count > 0 )
	{
		--clients[npc_id]->count;
		EVENT new_ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_MOVE_TARGET, 0 };
		add_timer( new_ev );
	}
	else
	{
		char mess[5] = "BYE!";
		send_chat_packet( clients[npc_id]->target, npc_id, mess );

		clients[npc_id]->is_active = false;
		clients[npc_id]->is_move = false;
		clients[npc_id]->sx = clients[npc_id]->x;
		clients[npc_id]->sy = clients[npc_id]->y;
		clients[npc_id]->count = RUN_RANGE;
		clients[npc_id]->target = NULL;
	}
}

void do_range_move( int npc_id )
{
	if ( 0 == clients.count( npc_id ) )
	{
		std::cout << "npc : " << npc_id << "dose not exsist" << std::endl;
		while ( true );
	}

	if ( false == is_NPC( npc_id ) )
	{
		std::cout << "id : " << npc_id << "is not npc" << std::endl;
		while ( true );
	}

	bool player_exists = false;
	for ( int i = 0; i < NPC_ID_START; ++i )
	{
		if ( 0 == clients.count( i ) ) continue;
		if ( true == is_near_NPC( i, npc_id ) )
		{
			player_exists = true;
			break;
		}
	}
	if ( false == player_exists )
	{
		clients[npc_id]->is_move = false;
		return;
	}

	SOCKETINFO* npc = clients[npc_id];
	set <int> old_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			old_view_list.insert( obj.second->id );
	}
RM:
	int x = npc->x;
	int y = npc->y;
	switch ( rand() % 5 ) {
	case 0: if ( y > 0 ) y--; break;
	case 1: if ( y < ( WORLD_HEIGHT - 1 ) ) y++; break;
	case 2: if ( x > 0 ) x--; break;
	case 3: if ( x < ( WORLD_WIDTH - 1 ) ) x++; break;
	case 4: break;
	}
	if ( true == is_Lava( x, y ) || false == is_range_move( npc_id, x, y ) ) goto RM;

	npc->x = x;
	npc->y = y;
	set <int> new_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			new_view_list.insert( obj.second->id );
	}
	for ( auto& pc : clients ) {
		if ( true == is_NPC( pc.second->id ) ) continue;
		if ( false == is_near( pc.second->id, npc_id ) ) continue;
		if ( false == is_Alive( npc_id ) ) continue;///
		send_pos_packet( pc.second->id, npc_id );
	}

	if ( clients[npc_id]->is_active == false )//다른 이벤트가 없으면
	{
		if ( is_npc_in_near( npc_id ) )
		{
			EVENT new_ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_MOVE_RANGE, 0 };
			add_timer( new_ev );
		}
	}
	else
	{
		clients[npc_id]->is_move = false;
	}
}

void do_random_move( int npc_id )
{
	if ( 0 == clients.count( npc_id ) )
	{
		std::cout << "npc : " << npc_id << "dose not exsist" << std::endl;
		while ( true );
	}

	if ( false == is_NPC( npc_id ) )
	{
		std::cout << "id : " << npc_id << "is not npc" << std::endl;
		while ( true );
	}

	bool player_exists = false;
	for ( int i = 0; i < NPC_ID_START; ++i )
	{
		if ( 0 == clients.count( i ) ) continue;
		if ( true == is_near_NPC( i, npc_id ) )
		{
			player_exists = true;
			break;
		}
	}
	if ( false == player_exists )
	{
		clients[npc_id]->is_active = false;
		return;
	}

	SOCKETINFO* npc = clients[npc_id];
	set <int> old_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			old_view_list.insert( obj.second->id );
	}
RM:
	int x = npc->x;
	int y = npc->y;
	switch ( rand() % 4 ) {
	case 0: if ( y > 0 ) y--; break;
	case 1: if ( y < ( WORLD_HEIGHT - 1 ) ) y++; break;
	case 2: if ( x > 0 ) x--; break;
	case 3: if ( x < ( WORLD_WIDTH - 1 ) ) x++; break;
	}
	if ( true == is_Lava( x, y ) ) goto RM;

	npc->x = x;
	npc->y = y;
	set <int> new_view_list;
	for ( auto& obj : clients ) {
		if ( true == is_near( npc_id, obj.second->id ) )
			new_view_list.insert( obj.second->id );
	}
	for ( auto& pc : clients ) {
		if ( true == is_NPC( pc.second->id ) ) continue;
		if ( false == is_near( pc.second->id, npc_id ) ) continue;
		if ( false == is_Alive( npc_id ) ) continue;///
		send_pos_packet( pc.second->id, npc_id );
	}

	if ( clients[npc_id]->count > 0 )
	{
		--clients[npc_id]->count;
		EVENT new_ev { npc_id,chrono::high_resolution_clock::now() + 1s, EV_MOVE, 0 };
		add_timer( new_ev );
	}
	else
	{
		char mess[5] = "BYE!";
		send_chat_packet( clients[npc_id]->target, npc_id, mess );

		clients[npc_id]->is_active = false;
		clients[npc_id]->count = RUN_RANGE;
		clients[npc_id]->target = NULL;
	}
}

void do_pass_over_chat( int chatter, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );

	char mess[100];
	string chattername = WstringTostring( clients[chatter]->name );
	chattername += " : ";

	strcpy( mess, chattername.c_str() );

	strcpy( mess + chattername.length(), &packet[2] );

	for ( auto& cl : clients )
	{
		if ( false == is_NPC( cl.first ) )
		{
			if ( chatter != cl.first )
			{
				send_global_chat_packet( cl.first, mess );
			}
		}
	}
}

void do_worker()
{
	while ( true ) {
		DWORD num_byte;
		ULONG key;
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over;

		GetQueuedCompletionStatus( g_iocp, &num_byte, ( PULONG_PTR )p_key, &p_over, INFINITE );
		//여기서 에러 코드를 받아야된다 우리의 클라는 시도때도없이 뻗는다

		SOCKET client_s = clients[key]->socket;

		if ( num_byte == 0 ) {
			if ( clients[key]->x != -1 && clients[key]->y != -1 )
				database->SetPos( clients[key]->name, clients[key]->x, clients[key]->y, clients[key]->level, clients[key]->exp );

			closesocket( client_s );
			clients.erase( key );

			for ( auto& cl : clients )
			{
				if ( false == is_NPC( cl.first ) )
					send_remove_player_packet( cl.first, key );
			}
			continue;
		}  // 클라이언트가 closesocket을 했을 경우

		OVER_EX* over_ex = reinterpret_cast< OVER_EX* > ( p_over );

		if ( EV_RECV == over_ex->evnet_type ) {
			if ( over_ex->net_buf[1] == CS_LOGIN )
				ProcessIDPacket( key, over_ex->net_buf );
			else if ( over_ex->net_buf[1] == CS_ATTACK )
			{
				if ( clients[key]->is_attack == false )
				{
					clients[key]->is_attack = true;
					ProcessBattlePacket( key, over_ex->net_buf );
					EVENT ev { key,chrono::high_resolution_clock::now() + 1s, EV_CAN_ATTACK, 0 };
					add_timer( ev );
				}
			}
			else if ( over_ex->net_buf[1] == CS_CHAT )
				do_pass_over_chat( key, over_ex->net_buf );
			else if ( over_ex->net_buf[1] == CS_TELEPORT )
				ProcessTeleportPacket( key, over_ex->net_buf );
			else
			{
				if ( clients[key]->is_move == false )
				{
					clients[key]->is_move = true;
					ProcessMovePacket( key, over_ex->net_buf );	//key가 보낸 이 패킷을 처리해라
					EVENT ev { key,chrono::high_resolution_clock::now() + 1s, EV_CAN_MOVE, 0 };
					add_timer( ev );
				}
			}

			DWORD flags = 0;
			memset( &over_ex->over, 0x00, sizeof( WSAOVERLAPPED ) );
			WSARecv( client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0 );
		}
		else if ( EV_SEND == over_ex->evnet_type )
		{
			delete over_ex;
		}
		else if ( EV_CAN_MOVE == over_ex->evnet_type )
		{
			clients[key]->is_move = false;
			delete over_ex;
		}
		else if ( EV_CAN_ATTACK == over_ex->evnet_type )
		{
			clients[key]->is_attack = false;
			delete over_ex;
		}
		else if ( EV_MOVE_RANGE == over_ex->evnet_type )
		{
			do_range_move( key );
			delete over_ex;
		}
		else if ( EV_MOVE == over_ex->evnet_type )
		{
			do_random_move( key );
			delete over_ex;
		}
		else if ( EV_MOVE_TARGET == over_ex->evnet_type )
		{
			clients[key]->is_move = true;
			do_target_move( key );
			delete over_ex;
		}
		else if ( EV_HEAL == over_ex->evnet_type )
		{
			player_heal( key );
			delete over_ex;
		}
		else if ( EV_PLAYER_MOVE_NOTIFY == over_ex->evnet_type )
		{
			int player_id = *( int* )( over_ex->net_buf );
			lua_State* L = clients[key]->L;
			clients[key]->vm_lock.lock();
			lua_getglobal( L, "event_player_move_notify" );
			lua_pushnumber( L, player_id );
			lua_pushnumber( L, clients[player_id]->x );
			lua_pushnumber( L, clients[player_id]->y );
			lua_pcall( L, 3, 0, 0 );
			clients[key]->vm_lock.unlock();
			delete over_ex;
		}
		else if ( EV_ATTACK == over_ex->evnet_type )
		{
			int npc_id = *( int* )( over_ex->net_buf );
			// 사방 적한테 데미지
			damage( npc_id, get_power_by_level( key ) );
			// 적이 아야!
			char mess[6] = "OUCH!";
			send_chat_packet( key, npc_id, mess );
			// 맞은 적 target을 나로! 하고 쫓아오도록
			clients[npc_id]->target = key;
			// 공격 메세지 띄우기
			delete over_ex;
		}
		else if ( EV_NPC_ATTACK == over_ex->evnet_type )
		{
			// 사방 적한테 데미지
			bool is_die = damage( clients[key]->target, key, get_power_by_level( key ) );
			// 적이 아야!
			char mess[6] = "OUCH!";
			send_chat_packet( clients[key]->target, clients[key]->target, mess );
			// 공격 메세지 띄우기
			if ( !is_die )
				add_npc_target_move( clients[key]->target, key );

			delete over_ex;
		}
		else if ( EV_REBIRTH == over_ex->evnet_type )
		{
			std::cout << "im back!!\n";
			clients[key]->hp = clients[key]->level * 10;
			clients[key]->is_active = false;
			clients[key]->is_alive = true;

			clients[key]->x = clients[key]->sx;
			clients[key]->y = clients[key]->sy;

			for ( auto& pc : clients )
			{
				if ( true == is_NPC( pc.second->id ) ) continue;
				if ( false == is_near( pc.second->id, key ) ) continue;
				send_put_player_packet( pc.second->id, key );
			}

			delete over_ex;
		}
		else
		{
			std::cout << "unknown event type : " << over_ex->evnet_type << std::endl;
			while ( true );
		}
	}
}

int lua_get_x_position( lua_State* L )
{
	int npc_id = ( int )lua_tonumber( L, -1 );
	lua_pop( L, 2 );
	int x = clients[npc_id]->x;
	lua_pushnumber( L, x );
	return 1;
}

int lua_get_y_position( lua_State* L )
{
	int npc_id = ( int )lua_tonumber( L, -1 );
	lua_pop( L, 2 );
	int y = clients[npc_id]->y;
	lua_pushnumber( L, y );
	return 1;
}

int lua_send_chat_packet( lua_State* L )
{
	int player_id = ( int )lua_tonumber( L, -3 );
	int chatter_id = ( int )lua_tonumber( L, -2 );
	char* mess = ( char* )lua_tostring( L, -1 );
	lua_pop( L, 4 );
	send_chat_packet( player_id, chatter_id, mess );
	return 0;
}

int lua_start_npc_random_move( lua_State* L )
{
	int player_id = ( int )lua_tonumber( L, -2 );
	int npc_id = ( int )lua_tonumber( L, -1 );
	lua_pop( L, 3 );
	add_npc_random_move( player_id, npc_id );
	return 0;
}

int lua_start_npc_target_move( lua_State* L )
{
	int player_id = ( int )lua_tonumber( L, -2 );
	int npc_id = ( int )lua_tonumber( L, -1 );
	lua_pop( L, 3 );
	add_npc_target_move( player_id, npc_id );
	return 0;
}

void Create_NPC()
{
	cout << "initialize npc...\r";
	{//boss
		clients[NPC_ID_START] = new SOCKETINFO;
		clients[NPC_ID_START]->id = NPC_ID_START;
		clients[NPC_ID_START]->x = 1;
		clients[NPC_ID_START]->y = 20;
		clients[NPC_ID_START]->sx = 1;
		clients[NPC_ID_START]->sy = 20;
		clients[NPC_ID_START]->socket = -1;
		clients[NPC_ID_START]->is_active = false;
		clients[NPC_ID_START]->is_alive = true;
		clients[NPC_ID_START]->is_move = false;
		clients[NPC_ID_START]->count = RUN_RANGE;
		clients[NPC_ID_START]->target = NULL;
		clients[NPC_ID_START]->hp = 100;
		clients[NPC_ID_START]->level = 5;

		clients[NPC_ID_START]->monster_type = MON_BOSS;

		lua_State* L = clients[NPC_ID_START]->L = luaL_newstate();
		luaL_openlibs( L );

		luaL_loadfile( L, "boss.lua" );
		lua_pcall( L, 0, 0, 0 );

		lua_getglobal( L, "set_npc_id" );
		lua_pushnumber( L, NPC_ID_START );
		lua_pcall( L, 1, 0, 0 );

		lua_register( L, "API_get_x_position", lua_get_x_position );
		lua_register( L, "API_get_y_position", lua_get_y_position );
		lua_register( L, "API_send_chat_packet", lua_send_chat_packet );
		lua_register( L, "API_start_npc_target_move", lua_start_npc_target_move );
	}

	for ( int npc_id = NPC_ID_START + 1; npc_id < NPC_ID_START + NUM_NPC; ++npc_id ) {
		clients[npc_id] = new SOCKETINFO;
		clients[npc_id]->id = npc_id;
		int x = rand() % WORLD_WIDTH;
		int y = rand() % WORLD_HEIGHT;
		while ( true == is_Lava( x, y ) )
		{
			x = rand() % WORLD_WIDTH;
			y = rand() % WORLD_HEIGHT;
		}
		clients[npc_id]->x = x;
		clients[npc_id]->y = y;
		clients[npc_id]->sx = x;
		clients[npc_id]->sy = y;

		clients[npc_id]->socket = -1;
		clients[npc_id]->is_active = false;
		clients[npc_id]->is_alive = true;
		clients[npc_id]->is_move = false;
		clients[npc_id]->count = RUN_RANGE;
		clients[npc_id]->target = NULL;

		if ( npc_id % 3 == 0 )	// rabbit
		{
			clients[npc_id]->monster_type = MON_RABBIT;
			clients[npc_id]->hp = 10;
			clients[npc_id]->level = 1;

			lua_State* L = clients[npc_id]->L = luaL_newstate();
			luaL_openlibs( L );

			luaL_loadfile( L, "rabbit.lua" );
			lua_pcall( L, 0, 0, 0 );

			lua_getglobal( L, "set_npc_id" );
			lua_pushnumber( L, npc_id );
			lua_pcall( L, 1, 0, 0 );

			lua_register( L, "API_get_x_position", lua_get_x_position );
			lua_register( L, "API_get_y_position", lua_get_y_position );
			lua_register( L, "API_send_chat_packet", lua_send_chat_packet );
			lua_register( L, "API_start_npc_random_move", lua_start_npc_random_move );
		}
		else if ( npc_id % 3 == 1 )	// zombie
		{
			clients[npc_id]->monster_type = MON_ZOMBIE;
			clients[npc_id]->hp = 30;
			clients[npc_id]->level = 3;

			lua_State* L = clients[npc_id]->L = luaL_newstate();
			luaL_openlibs( L );

			luaL_loadfile( L, "zombie.lua" );
			lua_pcall( L, 0, 0, 0 );

			lua_getglobal( L, "set_npc_id" );
			lua_pushnumber( L, npc_id );
			lua_pcall( L, 1, 0, 0 );

			lua_register( L, "API_get_x_position", lua_get_x_position );
			lua_register( L, "API_get_y_position", lua_get_y_position );
			lua_register( L, "API_send_chat_packet", lua_send_chat_packet );
			lua_register( L, "API_start_npc_target_move", lua_start_npc_target_move );
		}
		else	//scorpion
		{
			clients[npc_id]->monster_type = MON_SCORPION;
			clients[npc_id]->hp = 20;
			clients[npc_id]->level = 2;

			lua_State* L = clients[npc_id]->L = luaL_newstate();
			luaL_openlibs( L );

			luaL_loadfile( L, "scorpion.lua" );
			lua_pcall( L, 0, 0, 0 );

			lua_getglobal( L, "set_npc_id" );
			lua_pushnumber( L, npc_id );
			lua_pcall( L, 1, 0, 0 );

			lua_register( L, "API_get_x_position", lua_get_x_position );
			lua_register( L, "API_get_y_position", lua_get_y_position );
			lua_register( L, "API_send_chat_packet", lua_send_chat_packet );
			lua_register( L, "API_start_npc_target_move", lua_start_npc_target_move );

		}
	}
	cout << "initialize npc finished!\n";
}

void do_timer()
{
	while ( true )
	{
		timer_lock.lock();
		while ( true == timer_queue.empty() )
		{
			timer_lock.unlock();
			this_thread::sleep_for( 10ms );
			timer_lock.lock();
		}
		const EVENT& ev = timer_queue.top();
		if ( ev.wakeup_time > chrono::high_resolution_clock::now() )
		{
			timer_lock.unlock();
			this_thread::sleep_for( 10ms );
			continue;
		}

		EVENT p_ev = ev;
		timer_queue.pop();
		timer_lock.unlock();

		if ( true == clients[p_ev.obj_id]->is_alive || p_ev.event_type == EV_REBIRTH )
		{
			OVER_EX* over_ex = new OVER_EX;
			over_ex->evnet_type = ( EVENT_TYPE )p_ev.event_type;
			PostQueuedCompletionStatus( g_iocp, 1, p_ev.obj_id, &over_ex->over );
		}
	}
}

int main()
{
	wcout.imbue( std::locale( "korean" ) );
	srand( time( NULL ) );

	Create_NPC();

	WSADATA WSAData;
	WSAStartup( MAKEWORD( 2, 2 ), &WSAData );
	SOCKET listenSocket = WSASocket( AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED );
	SOCKADDR_IN serverAddr;
	memset( &serverAddr, 0, sizeof( SOCKADDR_IN ) );
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons( SERVER_PORT );
	serverAddr.sin_addr.S_un.S_addr = htonl( INADDR_ANY );
	::bind( listenSocket, ( struct sockaddr* ) & serverAddr, sizeof( SOCKADDR_IN ) );
	listen( listenSocket, 5 );
	SOCKADDR_IN clientAddr;
	int addrLen = sizeof( SOCKADDR_IN );
	memset( &clientAddr, 0, addrLen );
	SOCKET clientSocket;
	DWORD flags;

	database = new DataBase();

	g_iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, NULL, 0 );
	thread worker_thread { do_worker };
	thread worker_thread2 { do_worker };
	thread worker_thread3 { do_worker };
	thread timer_thread { do_timer };
	while ( true ) {
		clientSocket = accept( listenSocket, ( struct sockaddr* ) & clientAddr, &addrLen );
		int user_id = new_user_id++;

		SOCKETINFO* new_player = new SOCKETINFO;
		new_player->id = user_id;
		new_player->socket = clientSocket;
		new_player->recv_over.wsabuf[0].len = MAX_BUFFER;
		new_player->recv_over.wsabuf[0].buf = new_player->recv_over.net_buf;
		new_player->recv_over.evnet_type = EV_RECV;
		new_player->x = -1;
		new_player->y = -1;
		new_player->sx = -1;
		new_player->sy = -1;
		new_player->is_active = false;
		new_player->is_alive = true;
		new_player->is_move = false;
		new_player->name = L"_dummy";
		new_player->level = 5;
		new_player->exp = 0;
		new_player->hp = 20;

		clients.insert( make_pair( user_id, new_player ) );

		CreateIoCompletionPort( reinterpret_cast< HANDLE >( clientSocket ), g_iocp, user_id, 0 );

		memset( &clients[user_id]->recv_over.over, 0x00, sizeof( WSAOVERLAPPED ) );
		flags = 0;
		int ret = WSARecv( clientSocket, clients[user_id]->recv_over.wsabuf, 1, NULL,
			&flags, &( clients[user_id]->recv_over.over ), NULL );
		if ( 0 != ret ) {
			int err_no = WSAGetLastError();
			if ( WSA_IO_PENDING != err_no )
				error_display( "WSARecv Error :", err_no );
		}

	}
	worker_thread.join();
	timer_thread.join();
	closesocket( listenSocket );
	WSACleanup();
}
