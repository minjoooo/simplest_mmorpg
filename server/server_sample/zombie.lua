my_id = -1;

function set_npc_id(id)
	my_id = id;
end

function event_player_move_notify( player_id, x, y )
	my_x = API_get_x_position(my_id);
	my_y = API_get_y_position(my_id);

	dx = math.abs(x - my_x)
	dy = math.abs(y - my_y)

	if  ((dx+dy) < 4) then
		API_send_chat_packet(player_id, my_id, "brain!")
		API_start_npc_target_move(player_id, my_id)
	end
end
