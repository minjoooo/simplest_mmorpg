#include <string>
#include "DataBase.h"


DataBase::DataBase()
{
	retcode = SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv );
	retcode = SQLSetEnvAttr( henv, SQL_ATTR_ODBC_VERSION, ( SQLPOINTER* )SQL_OV_ODBC3, 0 );
	retcode = SQLAllocHandle( SQL_HANDLE_DBC, henv, &hdbc );	//명령어 저장할 핸들
	SQLSetConnectAttr( hdbc, SQL_LOGIN_TIMEOUT, ( SQLPOINTER )5, 0 );
	retcode = SQLConnect( hdbc, ( SQLWCHAR* )L"2017182006_GSP_7", SQL_NTS, ( SQLWCHAR* )NULL, SQL_NTS, NULL, SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		std::cout << "DB connect!\n";
	}
}
DataBase::~DataBase()
{
	SQLCancel( hstmt );
	SQLFreeHandle( SQL_HANDLE_STMT, hstmt );
	SQLDisconnect( hdbc );
	SQLFreeHandle( SQL_HANDLE_DBC, hdbc );
	SQLFreeHandle( SQL_HANDLE_ENV, henv );
}

void DataBase::GetPos( std::wstring id, int* posX, int* posY ,int* level, int* exp)
{
	std::wstring qu {};
	qu += L"EXEC Get_pos ";
	qu += id;

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );

	retcode = SQLExecDirect( hstmt, ( SQLWCHAR* )qu.c_str(), SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		retcode = SQLBindCol( hstmt, 1, SQL_C_LONG, &nPosX, 10, &cbPosX );
		retcode = SQLBindCol( hstmt, 2, SQL_C_LONG, &nPosY, 10, &cbPosY );
		retcode = SQLBindCol( hstmt, 3, SQL_C_LONG, &nLevel, 10, &cbLevel );
		retcode = SQLBindCol( hstmt, 4, SQL_C_LONG, &nExp, 10, &cbExp );

		retcode = SQLFetch( hstmt );	//SQLFetch를 사용해서 리턴값을 받는다
		if ( retcode == SQL_ERROR )
			show_error();
		if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
		{
			*posX = nPosX;
			*posY = nPosY;
			*level = nLevel;
			*exp = nExp;

			wprintf( L"Get : %d %d %d %d\n", nPosX, nPosY, nLevel, nExp );
		}
		else
		{
			*posX = -1;
			*posY = -1;
			*level = -1;
			*exp = -1;
			std::cout << "Get NoneID\n";
		}
	}
}

void DataBase::SetPos( std::wstring id, int posX, int posY, int level, int exp )
{
	std::wstring qu {};

	qu += L"EXEC Set_pos ";
	qu += id;
	qu += L", ";
	qu += std::to_wstring( posX );
	qu += L", ";
	qu += std::to_wstring( posY );
	qu += L", ";
	qu += std::to_wstring( level );
	qu += L", ";
	qu += std::to_wstring( exp );
	std::wcout << qu << std::endl;

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );
	retcode = SQLExecDirect( hstmt, ( SQLWCHAR* )qu.c_str(), SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		std::cout << "Set!\n";
	}
	else
		std::cout << "Set ERROR" << std::endl;
}

void DataBase::HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode )
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER iError;
	WCHAR wszMessage[1000];
	WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
	if ( RetCode == SQL_INVALID_HANDLE ) {
		fwprintf( stderr, L"Invalid handle!\n" );
		return;
	}
	while ( SQLGetDiagRec( hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		( SQLSMALLINT )( sizeof( wszMessage ) / sizeof( WCHAR ) ), ( SQLSMALLINT* )NULL ) == SQL_SUCCESS ) {
		// Hide data truncated..
		if ( wcsncmp( wszState, L"01004", 5 ) ) {
			fwprintf( stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError );
		}
	}
}
void DataBase::show_error()
{
	printf( "error\n" );
}