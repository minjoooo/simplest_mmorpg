#pragma once
#include <iostream>
#include <windows.h>  
#include <sqlext.h>  

constexpr auto NAMELEN = 11;

class DataBase
{
public:
	DataBase();
	~DataBase();

	void HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode );
	void show_error();

	void GetPos( std::wstring id, int* posX, int* posY, int* level, int* exp );
	void SetPos( std::wstring id, int posX, int posY, int level, int exp );

private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode;

	SQLINTEGER nPosX, nPosY, nLevel, nExp;
	SQLWCHAR szID[NAMELEN];
	SQLLEN cbID = 0, cbPosX = 0, cbPosY = 0, cbLevel = 0, cbExp = 0;
};

