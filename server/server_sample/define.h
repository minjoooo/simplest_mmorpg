#pragma once

#include "protocol.h"

#define MAX_BUFFER        1024

enum EVENT_TYPE { EV_RECV, EV_SEND, EV_MOVE, EV_PLAYER_MOVE_NOTIFY, EV_MOVE_TARGET, EV_ATTACK, EV_NPC_ATTACK, EV_HEAL, EV_REBIRTH, EV_CAN_MOVE };

struct OVER_EX {
	WSAOVERLAPPED	over;
	WSABUF			wsabuf[1];
	char			net_buf[MAX_BUFFER];
	EVENT_TYPE		evnet_type;
};

struct SOCKETINFO
{
	OVER_EX			recv_over;
	SOCKET			socket;
	int				id;
	int				hp;	//종류별로 다르게 해야한다
	bool			is_alive;

	bool			is_active;	// 이거를 player에서는 can move로 사용하자
	MONSTER_TYPE	monster_type;
	int				count;
	int				target;

	short			x, y;
	std::wstring	name;
	set <int>		near_id;
	mutex			near_lock;

	lua_State* L;
};

struct EVENT {
	int obj_id;
	chrono::high_resolution_clock::time_point wakeup_time;
	int event_type;
	int target_obj;	// 목표가 있어서 쫓아갈 때

	constexpr bool operator <( const EVENT& lh )const
	{
		return wakeup_time > lh.wakeup_time;
	}
};