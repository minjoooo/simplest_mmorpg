#pragma once
#include "protocol.h"

extern map <int, SOCKETINFO*> clients;

void send_packet( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	int packet_size = packet[0];

	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0x00, sizeof( OVER_EX ) );
	send_over->evnet_type = EV_SEND;
	memcpy( send_over->net_buf, packet, packet_size );
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;

	int ret = WSASend( clients[id]->socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0 );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			std::cout << "bbibbobbibbo" << std::endl;
			//error_display( "WSARecv Error :", err_no );
	}
}

void send_login_ok_packet( int id )
{
	sc_packet_login_ok packet;
	packet.id = id;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_OK;
	packet.x = clients[id]->x;
	packet.y = clients[id]->y;
	send_packet( id, &packet );
}

void send_login_deny_packet( int id )
{
	sc_packet_login_deny packet;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_DENY;
	send_packet( id, &packet );
}

void send_put_player_packet( int client, int new_id )
{
	sc_packet_put_player packet;
	packet.id = new_id;
	packet.size = sizeof( packet );
	packet.type = SC_PUT_PLAYER;
	packet.x = clients[new_id]->x;
	packet.y = clients[new_id]->y;
	packet.mon_type = clients[new_id]->monster_type;
	send_packet( client, &packet );

	if ( client == new_id ) return;
	lock_guard <mutex> lg { clients[client]->near_lock };
	clients[client]->near_id.insert( new_id );
}

void send_pos_packet( int client, int mover )
{
	sc_packet_pos packet;
	packet.id = mover;
	packet.size = sizeof( packet );
	packet.type = SC_POS;
	packet.x = clients[mover]->x;
	packet.y = clients[mover]->y;

	clients[client]->near_lock.lock();
	if ( 0 != clients[client]->near_id.count( mover ) ) {
		clients[client]->near_lock.unlock();
		send_packet( client, &packet );
	}
	else {
		clients[client]->near_lock.unlock();
		send_put_player_packet( client, mover );
	}
}

void send_remove_player_packet( int client, int leaver )
{
	sc_packet_remove_player packet;
	packet.id = leaver;
	packet.size = sizeof( packet );
	packet.type = SC_REMOVE_PLAYER;
	send_packet( client, &packet );

	lock_guard <mutex> lg { clients[client]->near_lock };
	clients[client]->near_id.erase( leaver );
}

void send_chat_packet( int client, int chatter, char  mess[] )
{
	sc_packet_chat packet;
	packet.id = chatter;
	packet.size = sizeof( packet );
	packet.type = SC_CHAT;
	strcpy_s( packet.chat, mess );
	send_packet( client, &packet );
}