#define _CRT_SECURE_NO_WARNINGS

#define SFML_STATIC 1
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <list>
#include <unordered_map>
#include <chrono>
#include <fstream>
#include <random>
using namespace std;
using namespace chrono;


#ifdef _DEBUG
#pragma comment (lib, "lib/sfml-graphics-s-d.lib")
#pragma comment (lib, "lib/sfml-window-s-d.lib")
#pragma comment (lib, "lib/sfml-system-s-d.lib")
#pragma comment (lib, "lib/sfml-network-s-d.lib")
#else
#pragma comment (lib, "lib/sfml-graphics-s.lib")
#pragma comment (lib, "lib/sfml-window-s.lib")
#pragma comment (lib, "lib/sfml-system-s.lib")
#pragma comment (lib, "lib/sfml-network-s.lib")
#endif
#pragma comment (lib, "freetype.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "ws2_32.lib")

#include "../../server/server_sample/protocol.h"

sf::TcpSocket socket;

constexpr auto SCREEN_WIDTH = 19;
constexpr auto SCREEN_HEIGHT = 19;

constexpr auto TILE_WIDTH = 64;
constexpr auto WINDOW_WIDTH = TILE_WIDTH * SCREEN_WIDTH;   // size of window
constexpr auto WINDOW_HEIGHT = TILE_WIDTH * SCREEN_HEIGHT;
constexpr auto BUF_SIZE = 200;
constexpr auto MAX_USER = 10;

int g_left_x;
int g_top_y;

int g_myid;

int g_myhp;
int g_myexp;
int g_mylevel;

bool is_chatting = false;
std::string chatMsg {};

sf::RenderWindow* g_window;
sf::Font g_font;

struct TextwTime {
	sf::Text text;
	high_resolution_clock::time_point time_out;
};

class ChatingCell {
private:
	bool m_showing;
	char m_mess[MAX_STR_LEN];
	list<TextwTime> m_text;
public:
	int m_x, m_y;
	ChatingCell() {
		m_showing = true;
		m_x = 10;
		m_y = 1000;
	}
	void draw() {
		if ( false == m_showing ) return;
		int h = 0;
		for ( list<TextwTime>::iterator iter = m_text.begin(); iter != m_text.end(); iter )
		{
			h -= 25;
			if ( high_resolution_clock::now() < iter->time_out ) {
				iter->text.setPosition( m_x, m_y + h );
				g_window->draw( iter->text );
				iter++;
			}
			else
				m_text.erase( iter++ );
		}
	}
	void add_chat( char chat[] ) {
		TextwTime tt;
		tt.text.setFont( g_font );
		tt.text.setString( chat );
		tt.time_out = high_resolution_clock::now() + 10s;

		m_text.emplace_front( tt );
	}
};

class OBJECT {
private:
	bool m_showing;
	sf::Sprite m_sprite;
	char m_mess[MAX_STR_LEN];
	high_resolution_clock::time_point m_time_out;
	sf::Text m_text;
public:
	int m_x, m_y;
	OBJECT( sf::Texture& t, int x, int y, int x2, int y2 ) {
		m_showing = false;
		m_sprite.setTexture( t );
		m_sprite.setTextureRect( sf::IntRect( x, y, x2, y2 ) );
		m_time_out = high_resolution_clock::now();
	}
	OBJECT() {
		m_showing = false;
		m_time_out = high_resolution_clock::now();
	}
	void show()
	{
		m_showing = true;
	}
	void hide()
	{
		m_showing = false;
	}

	void a_move( int x, int y ) {
		m_sprite.setPosition( ( float )x, ( float )y );
	}

	void a_draw() {
		g_window->draw( m_sprite );
	}

	void move( int x, int y ) {
		m_x = x;
		m_y = y;
	}
	void draw() {
		if ( false == m_showing ) return;
		float rx = ( m_x - g_left_x ) * 64.0f + 8;
		float ry = ( m_y - g_top_y ) * 64.0f + 8;
		m_sprite.setPosition( rx, ry );
		g_window->draw( m_sprite );
		if ( high_resolution_clock::now() < m_time_out ) {
			m_text.setPosition( rx - 10, ry - 10 );
			g_window->draw( m_text );
		}
	}
	void add_chat( char chat[] ) {
		m_text.setFont( g_font );
		m_text.setString( chat );
		m_time_out = high_resolution_clock::now() + 1s;
	}
};


OBJECT avatar;
OBJECT players[MAX_USER];
unordered_map <int, OBJECT> npcs;

OBJECT ground_tile[10];

sf::Texture* board;
sf::Texture* pieces;
sf::Texture* boss;

int World[800][800];

ChatingCell chatingCell;
sf::Text chat;
sf::Text curPlayerState;

void client_initialize()
{
	board = new sf::Texture;
	pieces = new sf::Texture;
	boss = new sf::Texture;
	if ( false == g_font.loadFromFile( "cour.ttf" ) ) {
		cout << "Font Loading Error!\n";
		while ( true );
	}
	board->loadFromFile( "ground.bmp" );
	pieces->loadFromFile( "character.png" );
	boss->loadFromFile( "boss.png" );
	for ( int i = 0; i < 10; ++i )
	{
		ground_tile[i] = OBJECT { *board, 64 * i, 0, TILE_WIDTH, TILE_WIDTH };
	}
	avatar = OBJECT { *pieces, 0, 0, 64, 64 };
	avatar.move( 4, 4 );

	for ( auto& pl : players )
	{
		pl = OBJECT { *pieces, 0, 0, 64, 64 };
	}

	ifstream in;
	in.open( "map.txt" );

	for ( int i = 0; i < 800; ++i )
	{
		for ( int j = 0; j < 800; ++j )
		{
			in >> World[i][j];
		}
	}
	in.close();

}

void client_finish()
{
	delete board;
	delete pieces;
	delete boss;
}

void ProcessPacket( char* ptr )
{
	static bool first_time = true;
	switch ( ptr[1] )
	{
	case SC_POS:
	{
		sc_packet_pos* my_packet = reinterpret_cast< sc_packet_pos* >( ptr );
		int other_id = my_packet->id;
		if ( other_id == g_myid ) {
			avatar.move( my_packet->x, my_packet->y );
			g_left_x = my_packet->x - 10;
			g_top_y = my_packet->y - 10;
		}
		else if ( other_id < MAX_USER ) {
			players[other_id].move( my_packet->x, my_packet->y );
		}
		else {
			if ( 0 != npcs.count( other_id ) )
				npcs[other_id].move( my_packet->x, my_packet->y );
		}
		break;
	}
	case SC_PLAYER_STATE_CHAGE:
	{
		sc_packet_set_player_state* my_packet = reinterpret_cast< sc_packet_set_player_state* >( ptr );
		switch ( my_packet->state_type )
		{
		case STATE_HP:
			g_myhp = my_packet->num;
			break;
		case STATE_EXP:
			g_myexp = my_packet->num;
			break;
		case STATE_LEVEL:
			g_mylevel = my_packet->num;
			g_myexp = 0;
			g_myhp = g_mylevel * 10;
			break;
		}
		break;
	}
	case SC_PUT_OBJECT:
	{
		sc_packet_put_object* my_packet = reinterpret_cast< sc_packet_put_object* >( ptr );
		int id = my_packet->id;

		if ( id == g_myid ) {
			avatar.move( my_packet->x, my_packet->y );
			g_left_x = my_packet->x - 10;
			g_top_y = my_packet->y - 10;
			avatar.show();
		}
		else if ( id < MAX_USER ) {
			players[id].move( my_packet->x, my_packet->y );
			players[id].show();
		}
		else {
			if ( my_packet->obj_type == MON_RABBIT )
			{
				npcs[id] = OBJECT { *pieces, 128, 0, 64, 64 };
			}
			else if ( my_packet->obj_type == MON_ZOMBIE )
			{
				npcs[id] = OBJECT { *pieces, 64, 0, 64, 64 };
			}
			else if ( my_packet->obj_type == MON_BOSS )
			{
				npcs[id] = OBJECT { *boss, 0, 0, 64, 64 };
			}
			else if ( my_packet->obj_type == MON_SCORPION )
			{
				npcs[id] = OBJECT { *pieces, 192, 0, 64, 64 };
			}
			npcs[id].move( my_packet->x, my_packet->y );
			npcs[id].show();
		}
		break;
	}
	case SC_REMOVE_OBJECT:
	{
		sc_packet_remove_object* my_packet = reinterpret_cast< sc_packet_remove_object* >( ptr );
		int other_id = my_packet->id;
		if ( other_id == g_myid ) {
			avatar.hide();
		}
		else if ( other_id < MAX_USER ) {
			players[other_id].hide();
		}
		else {
			//npcs.erase( other_id );
			npcs[other_id].hide();
		}
		break;
	}
	case SC_CHAT:
	{
		sc_packet_chat* my_packet = reinterpret_cast< sc_packet_chat* >( ptr );
		int other_id = my_packet->id;
		if ( other_id == g_myid ) {
			avatar.add_chat( my_packet->chat );
		}
		else if ( other_id < MAX_USER ) {
			players[other_id].add_chat( my_packet->chat );
		}
		else {
			if ( 0 != npcs.count( other_id ) )
				npcs[other_id].add_chat( my_packet->chat );
		}
		break;
	}
	case SC_GLOBAL_CHAT:
	{
		sc_packet_global_chat* my_packet = reinterpret_cast< sc_packet_global_chat* >( ptr );
		chatingCell.add_chat( my_packet->chat );
		break;
	}
	case SC_LOGIN_OK:
	{
		client_initialize();
		sc_packet_login_ok* packet = reinterpret_cast< sc_packet_login_ok* >( ptr );
		g_myid = packet->id;
		g_myhp = packet->hp;
		g_myexp = packet->exp;
		g_mylevel = packet->level;
		avatar.move( packet->x, packet->y );

		break;
	}
	case SC_LOGIN_FAIL:
	{
		std::cout << "ID not exist" << std::endl;
		system( "pause" );
		exit( 0 );

		break;
	}
	default:
		printf( "Unknown PACKET type [%d]\n", ptr[1] );
	}
}

void process_data( char* net_buf, size_t io_byte )
{
	char* ptr = net_buf;
	static size_t in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[BUF_SIZE];

	while ( 0 != io_byte ) {
		if ( 0 == in_packet_size ) in_packet_size = ptr[0];
		if ( io_byte + saved_packet_size >= in_packet_size ) {
			memcpy( packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size );
			ProcessPacket( packet_buffer );
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy( packet_buffer + saved_packet_size, ptr, io_byte );
			saved_packet_size += io_byte;
			io_byte = 0;
		}
	}
}

void client_main()
{
	char net_buf[BUF_SIZE];
	size_t	received;

	auto recv_result = socket.receive( net_buf, BUF_SIZE, received );
	if ( recv_result == sf::Socket::Error )
	{
		wcout << L"Recv 에러!";
		while ( true );
	}
	if ( recv_result != sf::Socket::NotReady )
		if ( received > 0 ) process_data( net_buf, received );

	for ( int i = 0; i < SCREEN_WIDTH; ++i )
		for ( int j = 0; j < SCREEN_HEIGHT; ++j )
		{
			int tile_x = i + g_left_x;
			int tile_y = j + g_top_y;
			if ( ( tile_x < 0 ) || ( tile_y < 0 ) ) continue;
			ground_tile[World[tile_x][tile_y]].a_move( TILE_WIDTH * i + 7, TILE_WIDTH * j + 7 );
			ground_tile[World[tile_x][tile_y]].a_draw();
		}

	avatar.draw();
	for ( auto& pl : players ) pl.draw();
	for ( auto& npc : npcs ) npc.second.draw();
	chatingCell.draw();
	{	//cur player state

		string temp_state {};
		temp_state += "HP : ";
		temp_state += to_string( g_myhp );
		temp_state += "\nLevel : ";
		temp_state += to_string( g_mylevel );
		temp_state += "\nEXP : ";
		temp_state += to_string( ( g_myexp * 100.f ) / ( pow( 2, g_mylevel ) * 100 ) );
		temp_state += "%";
		//여기에 exp per추가할꺼야

		curPlayerState.setFont( g_font );
		curPlayerState.setString( temp_state );
		curPlayerState.setPosition( 10, 25 );
		g_window->draw( curPlayerState );
	}
	if ( is_chatting )
	{
		chat.setFont( g_font );
		string tempchat {};
		tempchat += "/ ";
		tempchat += chatMsg;
		chat.setString( tempchat );
		chat.setPosition( 10, 1025 );
		g_window->draw( chat );
	}
}

void send_chat_packet( int p_type )
{
	cs_packet_chat packet;
	packet.size = sizeof( packet );
	packet.type = p_type;
	strcpy_s( packet.chat_str, chatMsg.c_str() );

	size_t sent = 0;
	socket.send( &packet, sizeof( packet ), sent );
}

void send_packet( int p_type )
{
	cs_packet_attack packet; // 다 똑같이 생겼으니깐 그냥 이걸로 일단 혀
	packet.size = sizeof( packet );
	packet.type = p_type;

	size_t sent = 0;
	socket.send( &packet, sizeof( packet ), sent );
}

void send_packet( int p_type, int move_type )
{
	cs_packet_move packet;
	packet.size = sizeof( packet );
	packet.type = p_type;
	packet.direction = move_type;

	size_t sent = 0;
	socket.send( &packet, sizeof( packet ), sent );
}

int main()
{
	std::string SERVERIP;
	std::cout << "SERVER IP : ";
	std::cin >> SERVERIP;

	char ID[11];
	std::cout << "ID : ";
	std::cin >> ID;

	wcout.imbue( locale( "korean" ) );
	sf::Socket::Status status = socket.connect( SERVERIP, SERVER_PORT );
	socket.setBlocking( false );

	if ( status != sf::Socket::Done ) {
		wcout << L"서버와 연결할 수 없습니다.\n";
		while ( true );
	}

	cs_packet_login packet;
	packet.size = strlen( ID ) + 2;
	packet.type = CS_LOGIN;
	size_t sent = 0;
	for ( int i = 0; i < strlen( ID ); ++i )
	{
		packet.id[i] = ID[i];
	}
	socket.send( &packet, sizeof( packet ), sent );

	//client_initialize();

	sf::RenderWindow window( sf::VideoMode( WINDOW_WIDTH, WINDOW_HEIGHT ), "2D CLIENT" );
	g_window = &window;

	while ( window.isOpen() )
	{
		sf::Event event;
		while ( window.pollEvent( event ) )
		{
			if ( event.type == sf::Event::Closed )
				window.close();
			if ( event.type == sf::Event::KeyPressed )
			{
				if ( is_chatting )
				{
					if ( event.key.code == sf::Keyboard::Enter )
					{
						is_chatting = false;

						string tmsg = "ME : " + chatMsg;

						char* cstr = new char[tmsg.length() + 1];
						strcpy( cstr, tmsg.c_str() );
						chatingCell.add_chat( cstr );
						delete[] cstr;

						send_chat_packet( CS_CHAT );

						chatMsg.clear();
					}
					else if ( event.key.code == 57 )
						chatMsg += ' ';
					else if ( event.key.code == 59 )
						chatMsg = chatMsg.substr( 0, chatMsg.length() - 1 );
					else
						chatMsg += ( char )( ( int )event.key.code + 97 );
				}
				else
				{
					int p_type = -1;
					int move_type = -1;
					switch ( event.key.code ) {
					case sf::Keyboard::Left:
						p_type = CS_MOVE;
						move_type = DIR_LEFT;
						break;
					case sf::Keyboard::Right:
						p_type = CS_MOVE;
						move_type = DIR_RIGHT;
						break;
					case sf::Keyboard::Up:
						p_type = CS_MOVE;
						move_type = DIR_UP;
						break;
					case sf::Keyboard::Down:
						p_type = CS_MOVE;
						move_type = DIR_DOWN;
						break;
					case sf::Keyboard::A:
						p_type = CS_ATTACK;
						break;
					case sf::Keyboard::Enter:
						is_chatting = !is_chatting;
						break;
					case sf::Keyboard::Escape:
						window.close();
						break;
					}
					if ( -1 != p_type )
					{
						if ( -1 != move_type )
							send_packet( p_type, move_type );
						else
							send_packet( p_type );
					}

				}
			}
		}

		window.clear();
		client_main();
		window.display();
	}
	client_finish();

	return 0;
}